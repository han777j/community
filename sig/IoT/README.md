# IoT兴趣小组（SIG）

IoT兴趣小组（SIG） 围绕边缘计算、AIoT为核心技术，目标为openKylin社区拓展物联网产业场景。

## 工作目标

- 满足爱好者在openkylin中快速实现物联设备接入与管理，快速开发物联网场景。

## 维护包列表

Shifu

## SIG 成员

### Owner

- 边无际(info@edgenesis.com)

### Maintainers

- 陈永立（yonglichen@edgenesis.com）
- 李翔(lixiang@edgenesis.com)
- 秦小禹（xqin@edgenesis.com）
- 郑凯文（kevin@edgenesis.com）
- 郭琦（qguo@edgenesis.com）

## 邮件列表

- [iot@lists.openkylin.top](mailto:iot@lists.openkylin.top)
