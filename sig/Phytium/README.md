## Phytium SIG小组

Phytium SIG对搭载Phytium高性能处理器的平台提供持续的内核适配支持与性能优化。

## 工作目标

1. 维护已合入分支的飞腾内核补丁代码、完善飞腾产品线支持、提交内核驱动相关补丁。
2. 开发、优化、适配飞腾自研开源软件。
3. 组建互认证联合实验室，在SIG中同步联合实验室互认证成果。
4. 整合飞腾产业合作伙伴，实现组内成员结构立体化，资源及解决方案多样化。

## SIG成员

## Owner
毛泓博 (`maohongbo@phytium.com.cn`)

## Maintainers
帅家坤 (`shuaijiakun1288@phytium.com.cn`)

## SIG交付件列表

- 内核补丁合入开发分支
- Phytium定制版ISO
- Phytium开源软件仓

## SIG邮件列表
phytium@lists.openkylin.top
