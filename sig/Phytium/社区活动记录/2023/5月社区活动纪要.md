# Phytium SIG 在 openKylin 社区活动中的贡献记录：2023年5月

## “openKylin高校沙龙”湖南大学站
2023年5月10日，“openKylin高校沙龙”湖南大学站在湖南大学信科院院楼314教室成功举办，飞腾软件支持部高级经理毛泓博参与了此次沙龙。
在活动中，毛泓博带来的《飞腾工作内容与职能简介》主题分享，给同学们介绍了飞腾公司的产品体系和最新成果，并以飞腾为例带领大家分别了解软硬件的开发流程。

![毛泓博在“openKylin高校沙龙”湖南大学站](https://www.openkylin.top/upload/202305/1683875148335944.jpeg)

## “openKylin高校沙龙”中南大学站
2023年5月14日，“openKylin高校沙龙”中南大学站成功举办，飞腾软件支持部高级经理毛泓博参与了此次沙龙。
在活动中，毛泓博带来的《飞腾工作内容与职能简介》主题分享，给同学们介绍了飞腾公司的产品体系和最新成果，并以飞腾为例带领大家分别了解软硬件的开发流程。
![毛泓博在“openKylin高校沙龙”中南大学站](https://www.openkylin.top/upload/202305/1684216453803390.png)